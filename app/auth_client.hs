{-# LANGUAGE OverloadedStrings #-}

module Main where

import Network.RADIUS.Client
import Network.RADIUS.Types
import Control.Monad.IO.Class    (liftIO)

main :: IO ()
main = do
  putStrLn "Hello, RADIUS client!"
  withCtx "radius://[::]:1812" "secret" $ \ c -> do
    sendAuth c 1000000 [CallingStationIdAttribute "chaos"] >>= liftIO . print
    sendAuth c 1000000 [CallingStationIdAttribute "yura"] >>= liftIO . print
    return ()
