{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main where

import Data.ByteString.Lazy       (ByteString)
import Data.ByteString.Lazy.Char8 (pack)
import Control.Monad.IO.Class     (MonadIO, liftIO)
import Network.RADIUS.Server
import Options.Applicative
import Colog.Core

data Opts = Opts
          { optUri :: !String
          , optSecret   :: !ByteString
          }

parseOpts :: IO Opts
parseOpts =
  execParser $ info
                  (optsParser <**> helper)
                  (fullDesc <> header "Debugging RADIUS Accounting server")

optsParser :: Parser Opts
optsParser =
  Opts <$> option str
       ( long "listen" <> metavar "URI"
       <> help "Listen on the UDP address:port"
       <> showDefault <> value "radius://127.0.0.1:1813"
       )
       <*> fmap pack (option str
       ( long "secret" <> metavar "PASSWORD"
       <> help "RADIUS Accounting server shared secret")
       )

fmtMessage :: Message -> String
fmtMessage (Message s msg) =  "[" <> show s <> "]: " ++ msg

logStdOut :: MonadIO m => LogAction m Message
logStdOut = cmap fmtMessage logStringStdout

main :: IO ()
main = do
  Opts{..} <- parseOpts
  runAcctServer (Config optUri optSecret logStdOut) $ \ p -> do
    liftIO $ print p
    return . Just $ mkAcctResponse p

