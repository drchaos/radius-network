{-# LANGUAGE OverloadedStrings #-}

module Main where

import Network.RADIUS.Client

main :: IO ()
main = do
  putStrLn "Hello, RADIUS client!"
  withCtx "radius://[::1]" "secret" $ \ c -> do
    _ <- sendAccountingInfo c 1000000 []
    _ <- sendAccountingInfo c 1000000 []
    return ()
