{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Network.RADIUS.Client where

import Network.RADIUS.Encoding (sign)
import Network.RADIUS.Internal
import Network.RADIUS.Types

import Control.Lens              (view)
import Control.Lens.Extras       (is)
import Control.Monad             (void, unless)
import Control.Monad.Catch       (Exception, MonadMask, MonadThrow, bracket, throwM)
import Control.Monad.IO.Class    (MonadIO, liftIO)
import Data.Binary               (decode, encode)
import Data.ByteString.Lazy      (ByteString, fromStrict, toStrict)
import Data.IORef                (IORef, atomicModifyIORef', newIORef)
import Data.Word                 (Word8)
import Network.Socket
    ( AddrInfo(..)
    , AddrInfoFlag(..)
    , SockAddr
    , Socket
    , SocketType(..)
    , close
    , defaultHints
    , getAddrInfo
    , socket
    )
import Network.Socket.ByteString (recvFrom, sendTo)
import System.Timeout            (timeout)

data RadiusContextException = WrongUri String
                            deriving Show

data RadiusClientError = ResponseTimeout
                       | AuthenticationFailed
                       deriving Show

instance Exception RadiusContextException
instance Exception RadiusClientError

data Context = Ctx { addr     :: SockAddr
                   , secret   :: ByteString
                   , sock     :: Socket
                   , packetId :: IORef Word8
                   }

createCtx :: (MonadIO m, MonadThrow m) => String -> ByteString -> m Context
createCtx uriStr secret = do
  (host, port) <- either (throwM . WrongUri) return $ parseRADIUSUri uriStr
  let hints = defaultHints { addrFlags = [AI_NUMERICSERV]
                           , addrSocketType = Datagram
                           }
  ai <- liftIO $ head <$> getAddrInfo (Just hints) (Just host) (Just port)
  let addr = addrAddress ai
  sock     <- liftIO $ socket (addrFamily ai) (addrSocketType ai) (addrProtocol ai)
  packetId <- liftIO $ newIORef 1
  return $ Ctx {..}

freeCtx :: MonadIO m => Context -> m ()
freeCtx = liftIO . close . sock

withCtx :: (MonadIO m, MonadMask m)
       => String -> ByteString -> (Context -> m b) -> m b
withCtx uri secret = bracket (createCtx uri secret) freeCtx

sendAccountingInfo :: (MonadIO m, MonadThrow m)
       => Context -> Int -> [PacketAttribute] -> m Packet
sendAccountingInfo c@Ctx{..} duration attrs = do
  pId    <- liftIO $ atomicModifyIORef' packetId $ \ i -> (i+1,i)
  let packet = Packet header attrs
      header = Header AccountingRequest pId 0 zeroAuthenticator
  sendPacket c duration (is _AccountingResponse . view (getHeader . getPacketType)) packet

sendAuth :: (MonadIO m, MonadThrow m)
       => Context -> Int -> [PacketAttribute] -> m Packet
sendAuth c@Ctx{..} duration attrs = do
  pId    <- liftIO $ atomicModifyIORef' packetId $ \ i -> (i+1,i)
  let packet = Packet header attrs
      header = Header AccessRequest pId 0 zeroAuthenticator
  sendPacket c duration (is _AccessAccept . view (getHeader . getPacketType)) packet

sendPacket :: (MonadIO m, MonadThrow m)
       => Context -> Int -> (Packet -> Bool) -> Packet -> m Packet
sendPacket Ctx{..} duration isExpected packet = do
  let bytes  = sign (encode packet) secret
  liftIO $ void $ sendTo sock (toStrict bytes) addr
  waitingForResponce (_getPacketId $ _getHeader packet) (getAuthenticator bytes)
  where
    throwNothing e = maybe (throwM e) return
    waitingForResponce origId origAuth = do
      (bResp,_) <- liftIO $   throwNothing ResponseTimeout
                          =<< timeout duration (recvFrom sock 4096)
    
      let accResp = decode @Packet resp
          resp    = fromStrict bResp
      if _getPacketId (_getHeader accResp ) == origId && isExpected accResp
         then do unless (checkAuth origAuth secret resp) $ throwM AuthenticationFailed
                 return accResp
         else waitingForResponce origId origAuth -- skip packet and retry

