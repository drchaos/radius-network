{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Network.RADIUS.Internal
( checkAuth
, getAuthenticator
, zeroAuthenticator
, parseRADIUSUri
) where

import Network.RADIUS.Encoding (sign)
import Network.URI             (URI(..), URIAuth(..), parseURI)

import qualified Data.ByteString.Lazy.Char8 as LB

type Secret = LB.ByteString
type Authenticator = LB.ByteString

checkAuth :: Authenticator -> Secret -> LB.ByteString -> Bool
checkAuth authenticator secret bytes =
  getAuthenticator bytes == getAuthenticator bytes'
    where
      bytes' = sign (LB.take 4 bytes <> authenticator <> LB.drop 20 bytes) secret

getAuthenticator :: LB.ByteString -> Authenticator
getAuthenticator = LB.take 16 . LB.drop 4

zeroAuthenticator :: Authenticator
zeroAuthenticator = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"

parseRADIUSUri :: String -> Either String (String, String)
parseRADIUSUri uriStr =
  case parseURI uriStr of
    Nothing                   -> Left uriStr
    Just URI{..}
     | uriScheme /= "radius:" -> Left $ "Expect 'radius' got : " ++ uriScheme
    Just uri ->
      case uriAuthority uri of
        Nothing               -> Left "No host specified."
        Just URIAuth{..}      -> do
          let host = if head uriRegName == '[' && last uriRegName == ']'
                        then tail . init $ uriRegName
                        else uriRegName
              port = if null uriPort
                        then "1813"
                        else tail uriPort
          Right (host, port)
