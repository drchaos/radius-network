{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Network.RADIUS.Server
( module Network.RADIUS.Server
, parseRADIUSUri
, Severity(..)
, LogAction(..)
) where

import Colog.Core
import Control.Concurrent.Lifted   (fork)
import Control.Monad               (forever, void, when)
import Control.Monad.Catch         (Exception, MonadMask, SomeException, bracket, catch, throwM)
import Control.Monad.IO.Class      (MonadIO, liftIO)
import Control.Monad.Trans.Control (MonadBaseControl)
import Data.Bifunctor              (first, second)
import Data.Binary                 (Binary, decodeOrFail, encode)
import Data.Coerce                 (coerce)
import Network.RADIUS.Encoding     (sign)
import Network.RADIUS.Internal     (checkAuth, parseRADIUSUri, zeroAuthenticator)
import Network.RADIUS.Types        (Header(..), Packet(..), PacketType(..))
import Network.Socket
    ( AddrInfo(..)
    , AddrInfoFlag(..)
    , Family(..)
    , Socket
    , SocketOption(..)
    , SocketType(..)
    , bind
    , close
    , defaultHints
    , getAddrInfo
    , setSocketOption
    , socket
    )
import Network.Socket.ByteString   (recvFrom, sendAllTo)

import qualified Data.ByteString.Lazy.Char8 as LB

data RadiusSrvException = WrongUri String
                        | MalformedPacket String
                        deriving Show

instance Exception RadiusSrvException

data Message = Message Severity String
type Logger m = LogAction m Message


-------------------

type Application = forall m. (MonadIO m) => Packet -> m (Maybe Packet)

maxRADIUSMsgSize :: Int
maxRADIUSMsgSize = 512

decodeEither :: (Binary a) => LB.ByteString -> Either String a
decodeEither = first (\(_,_,v)->v) . second (\(_,_,v) -> v) .  decodeOrFail

decodePacket :: LB.ByteString -> Either RadiusSrvException Packet
decodePacket bytes = first MalformedPacket $ decodeEither bytes

mkAcctResponse :: Packet -> Packet
mkAcctResponse (Packet (Header _ messageId _ authenticator) _) =
  Packet (Header AccountingResponse messageId 16 authenticator) []

data Config = Config
            { uri    :: String
            , secret :: LB.ByteString
            , logger :: forall m. (MonadIO m) => Logger m
            }

writeTo :: LogAction m Message -> Severity -> String -> m ()
writeTo l s = coerce l . Message s

mkAcctApp :: Config -> Application -> LB.ByteString -> Application
mkAcctApp Config{..} app bytes packet =
  if checkAuth zeroAuthenticator secret bytes
     then app packet
     else do writeTo logger I $ "Packet " <> (show . _getPacketId . _getHeader) packet
                                          <> " authentication failed. Ignoring."
             return Nothing

runServer :: (MonadMask m, MonadIO m, MonadBaseControl IO m)
          => Config -> (LB.ByteString -> Packet -> m (Maybe Packet)) -> m b
runServer c@Config{..} app = do
  addrPair <- either (throwM . WrongUri) return $ parseRADIUSUri uri
  bracket (bindListener addrPair) (liftIO.close) $ \sk ->
    runServerWith sk c app
  where
    bindListener (host, port) = liftIO $ do
      let hints = defaultHints { addrFlags = [AI_NUMERICSERV]
                               , addrSocketType = Datagram
                               }
      ai <- head <$> getAddrInfo (Just hints) (Just host) (Just port)
      let addr = addrAddress ai
      sock     <- socket (addrFamily ai) (addrSocketType ai) (addrProtocol ai)
      when (addrFamily ai == AF_INET6) $ setSocketOption sock IPv6Only 0
      bind sock addr `catch` bindFailure sock
      return sock
    bindFailure sk (e::SomeException) = do
      writeTo logger E $ "Failed to bind port to " ++ show uri ++ ": " ++
             show (e::SomeException)
      close sk

runServerWith :: (MonadMask m, MonadIO m, MonadBaseControl IO m)
              => Socket -> Config -> (LB.ByteString -> Packet -> m (Maybe Packet)) -> m b
runServerWith sock Config{..} app = forever $ do
  (bytes, addr) <- liftIO $ first LB.fromStrict <$> recvFrom sock maxRADIUSMsgSize
  writeTo logger D $ "Received bytes" ++ show bytes ++ " from: " ++ show addr
  case decodePacket bytes of
    Left e ->
      writeTo logger I $ "Radius packet is invalid according to rfc: "
                <> show e <> ", ignoring"
    Right packet ->
      void $ fork $ do
        writeTo logger D $ "Decoded RADIUS packet" ++ show packet
        app bytes packet >>= mapM_ (sendReply sock addr)
  where
    sendReply sk addr p  = do
      let bytes = encodePacket p
      writeTo logger D $ "Send RADIUS response:" <> show p <> " bytes: "
                       <> show bytes <> " to: " <> show addr
      liftIO $ sendAllTo sk bytes addr
    encodePacket = LB.toStrict . flip sign secret . encode
